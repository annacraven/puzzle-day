Rails.application.routes.draw do
  resources :quizzes do
    resources :questions, only: %i(index new create)
    resources :teams, only: %i(index new create show destroy) do
      resources :guesses, only: %i(create)
    end
  end
  
  root to: 'quizzes#index'
end
