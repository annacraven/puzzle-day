class QuestionsController < ApplicationController
  before_action :set_question, only: [:edit, :update, :destroy]
  before_action :set_quiz


  # GET /questions
  # GET /questions.json
  def index
    @questions = Question.where(quiz_id: @quiz).order(number: :asc)
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
  end

  # GET /questions/new
  def new
    @question = Question.new
  end

  # GET /questions/1/edit
  # def edit
  # end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params)
    if @question.save
      redirect_to quiz_questions_path(@quiz)
    else
      redirect_to quiz_questions_path(@quiz), notice: 'Question could not be created'
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  # def update
  #   respond_to do |format|
  #     if @question.update(question_params)
  #       format.html { redirect_to @question, notice: 'Question was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @question }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @question.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /questions/1
  # # DELETE /questions/1.json
  # def destroy
  #   @question.destroy
  #   respond_to do |format|
  #     format.html { redirect_to quiz_questions_url)(@quiz), notice: 'Question was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    def set_quiz
      @quiz = Quiz.find(params[:quiz_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.permit(:number, :title, :answer, :quiz_id)
    end
end
