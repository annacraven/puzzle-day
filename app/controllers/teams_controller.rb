class TeamsController < ApplicationController 
  before_action :set_quiz
  before_action :set_team, only: %i(show destroy)

  def index
    @teams = Team.where(quiz_id: @quiz).order(score: :desc, updated_at: :asc)
  end

  def new
    @team = Team.new
  end

  def create
    @team = Team.new(team_params)
    @team.answers = [0]*15 # if have time fix this
    if @team.save
      redirect_to quiz_teams_path(@quiz)
    else
      redirect_to quiz_teams_path(@quiz), notice: 'Team could not be created'
    end
  end

  def show; end

  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to quiz_teams_url(@quiz), notice: 'Team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_quiz
      @quiz = Quiz.find(params[:quiz_id])
    end

    def set_team
      @team = Team.find(params[:id])
    end

    def team_params
      params.permit(:name, :quiz_id)
    end

end