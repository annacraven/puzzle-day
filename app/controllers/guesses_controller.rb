class GuessesController < ApplicationController
  before_action :set_team

  def create
    @guess = Guess.new(guess_params)
    if @guess.save
      redirect_to quiz_team_path(@quiz, @team), notice: update_score
    else
      redirect_to quiz_team_path(@quiz, @team), notice: 'Error'
    end
  end

  private
    def set_team
      @quiz = Quiz.find(params[:quiz_id])
      @team = Team.find(params[:team_id])
    end

    def guess_params
      params.permit(:team_id, :question_id, :submission)
    end

    def update_score
      question = Question.find(@guess.question_id)
      if @guess.submission.strip.downcase == question.answer.strip.downcase
        team = Team.find(@guess.team_id)
        team.answers[question.number] = 1
        team.score = sum(team.answers)
        team.save
        return nil
      end
      wrong_response[rand(wrong_response.length)]
    end

    def sum(array)
      res = 0
      array.each { |x| res += x.to_i }
      res
    end

    def wrong_response
      [ 'Nope.',
        'Try again.',
        'Are you just guessing randomly?',
        "C'mon son.",
        'Did you have a brain tumor for breakfast?',
        'Just sit there in your wrongness and be wrong.',
        'Rubbish!',
        'Nah man.',
        'No one will blame you for giving up. In fact, quitting at this point is a perfectly reasonable response.',
        'Incorrect.',
        'https://www.youtube.com/watch?v=WrjwaqZfjIY',
        'Not quite it, champ.',
        "You'll never solve them all!"

      ]
    end
end