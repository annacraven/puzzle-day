class Team < ApplicationRecord
  belongs_to :quiz
  has_many :guesses, dependent: :destroy

  # 0 for wrong, 1 for correct. score is sum of answers array
  # comment out for deploy. if have time, set using env != production
  if Rails.env.development?
    serialize :answers, Array
  end
end
