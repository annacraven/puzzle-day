class Question < ApplicationRecord
  belongs_to :quiz
  has_many :guesses, dependent: :destroy

  validates :title, presence: true
  validates :answer, presence: true
  validates :number, presence: true,
                     numericality: { only_integer: true, less_than: 15, greater_than: 0 },
                     uniqueness: { scope: :quiz }
  validates :quiz, presence: true
end
