class Guess < ApplicationRecord
  belongs_to :team
  belongs_to :question

  validates :team, presence: true
  validates :question, presence: true
end
