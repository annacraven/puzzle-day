class Quiz < ApplicationRecord
  has_many :questions, dependent: :destroy
  has_many :teams, dependent: :destroy

  validates :title, presence: true
end
