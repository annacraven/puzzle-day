class UpdateTeams < ActiveRecord::Migration[5.2]
  def change
    add_column :teams, :answers, :text, array: true, default: [].to_yaml
    change_column :teams, :score, :integer, default: 0
  end
end
