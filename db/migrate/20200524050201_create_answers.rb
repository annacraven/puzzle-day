class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :guesses do |t|
      t.string :submission
      t.references :team, foreign_key: true
      t.references :question, foreign_key: true

      t.timestamps
    end
  end
end
